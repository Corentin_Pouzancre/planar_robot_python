# P2 answer

## Merge Comments

Comment on the differences between your `p1_results` and the ones given for this practical.

## Result Comments

Are your results identical to the ones demanded in **practical 2**? If not, why?

# P3 answer

## Merge Comments

## Result Comments
le test avec le sript.bash ne fonctionne pas, mais en lancant l'executable high_level_manager et trajectory generator, et en envoyant manuellement les commandes d'initialisation et de lancement, le programme fonctionne bien, on peut voir que les trajectoire en x et y suivent les points décrit dans le fichier csv.
# P4 answer

## Merge Comments
la methode que j'ai utilisé pour programmer cette node est différente de la votre, notemment pour la facon dont on parcours les valeurs du fichier CSV. dans mon programme, j'ai utilisé une boucle qui attend qu'on soit arrivé à la première position avant d'initialiser la valeurs suivante et de faire le déplacement.
## Result Comments

# P5 answer
je ne pariens pas a lancer le script p5.sh, il me dit qu'il manque pas mal de fichier qui sont pourtant créé.
sinon, j'arrive a lancer les deux nodes controller et simulator, il ne semble pas y avoir d'erreurs au lancement
## Merge Comments

## Result Comments

# P6 answer

## Merge Comments
les programmes etaient plus ou moins similaire dans la forme, j'avais définit moins de parametres que le code du professeur mais l'idee de programmation etait similaire 
## Result Comments
j'ai réussi a simuler des perturbation de vitesse, et afficher le robot grace a rvizz.
on peut voir que le robot est constamment en mouvement à cause des perturbation mais qu'il oscille autour du point désiré. c'est parce que la perturbation est un sinus.
si l'amplitude est trop eleve, le correcteur ne parviens plus à asservir la position et le robot commence à faire n'importe quoi. 
