import rclpy
from rclpy.node import Node
import math
from sensor_msgs.msg import JointState
from custom_messages.msg import KinData
from custom_messages.msg import CartesianState
from .position_interpolation.position_interpolation import PositionInterpolation
import numpy as np


class Disturbance(Node):
    def __init__(self):
        super().__init__('qdot_disturbance')

        # Définition du publisher
        self.publisher_ = self.create_publisher(
            JointState,
            'qdot_disturbance',
            10)

        # Paramètres du timer :
        self.sampling_period = 1e-2
        self.timer = self.create_timer(self.sampling_period, self.timer_callback)
        self.t = 0

        # Paramètres des perturbations :
        self.A = 0.5
        self.w = 100.0

    def timer_callback(self):
        self.t += self.sampling_period
        qdot_disturbance = JointState()
        qdot_disturbance.velocity = [self.A * math.sin(self.w * self.t),self.A * math.sin(self.w * self.t)]  
        self.publisher_.publish(qdot_disturbance)




def main(args=None):
    rclpy.init(args=args)
    disturbance_node = Disturbance()
    rclpy.spin(disturbance_node)
    disturbance_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()